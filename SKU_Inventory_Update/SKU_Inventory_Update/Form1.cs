﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Configuration;
using Npgsql;
using System.Threading;

namespace SKU_Inventory_Update
{
    public partial class Form1 : Form
    {
        NpgsqlConnection NpgsqlConn = new NpgsqlConnection(ConfigurationManager.AppSettings["DBConn"]);

        int iClientID = Int32.Parse(ConfigurationManager.AppSettings["ClientID"].ToString());
        int iLocationID = Int32.Parse(ConfigurationManager.AppSettings["LocationID"].ToString());

        Thread nThread = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            nThread = new Thread(UpdateSKUDetails1);
            nThread.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (nThread != null && nThread.IsAlive)
                nThread.Abort();
        }

        private void UpdateSKUDetails()
        {
            try
            {
                NpgsqlConn.Open();
                

                DataSet Ds = new DataSet();
                NpgsqlCommand cmd = new NpgsqlCommand("SELECT P.product_id FROM product P LEFT JOIN location_product_map LPM ON LPM.product_id = P.product_id WHERE P.client_id = " + iClientID + " AND LPM.location_id = " + iLocationID + " AND P.is_active = 'true' ORDER BY P.product_id", NpgsqlConn);
                cmd.CommandTimeout = 300;

                NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
                da.Fill(Ds);
                da.Dispose();

                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtRow in Ds.Tables[0].Rows)
                    {
                        int iProductID = Convert.ToInt32(dtRow["product_id"].ToString());

                        DataSet Ds1 = new DataSet();
                        NpgsqlCommand cmd1 = new NpgsqlCommand("SELECT SKU.product_id, SKU.mrp_id, SKU.stock_keeping_unit_id FROM stock_keeping_unit SKU LEFT JOIN product P ON P.product_id = SKU.product_id WHERE P.product_id = " + iProductID + ";", NpgsqlConn);
                        cmd1.CommandTimeout = 300;

                        NpgsqlDataAdapter da1 = new NpgsqlDataAdapter(cmd1);
                        da1.Fill(Ds1);
                        da1.Dispose();

                        if (Ds1 != null && Ds1.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dtRow1 in Ds1.Tables[0].Rows)
                            {
                                int iProductID1 = Convert.ToInt32(dtRow1["product_id"].ToString());
                                int iMrpID = Convert.ToInt32(dtRow1["mrp_id"].ToString());
                                int iSkuID = Convert.ToInt32(dtRow1["stock_keeping_unit_id"].ToString());

                                DataSet Ds2 = new DataSet();
                                NpgsqlCommand cmd2 = new NpgsqlCommand("SELECT gd.product_id, gd.mrp_id, g.location_id, gd.rate FROM goods_received_note_detail gd LEFT JOIN goods_received_note g ON g.goods_received_note_id = gd.goods_received_note_id WHERE gd.product_id = " + iProductID + " AND gd.mrp_id = " + iMrpID + " AND gd.modified_on = (SELECT modified_on FROM goods_received_note_detail WHERE product_id = " + iProductID + " AND gd.mrp_id = " + iMrpID + " order by modified_on DESC LIMIT 1) AND g.location_id = " + iLocationID + ";", NpgsqlConn);
                                cmd2.CommandTimeout = 300;

                                NpgsqlDataAdapter da2 = new NpgsqlDataAdapter(cmd2);
                                da2.Fill(Ds2);
                                da2.Dispose();

                                if (Ds2 != null && Ds2.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dtRow2 in Ds2.Tables[0].Rows)
                                    {
                                        float fRate = float.Parse(dtRow2["rate"].ToString());

                                        NpgsqlCommand cmd3 = new NpgsqlCommand("UPDATE location_stock_keeping_unit_map SET actual_landing_price = " + fRate + ", modified_on = now() at time zone 'utc' WHERE stock_keeping_unit_id = " + iSkuID + " AND location_id = " + iLocationID + ";", NpgsqlConn);
                                        cmd3.CommandTimeout = 300;
                                        cmd3.ExecuteNonQuery();
                                    }                                    
                                }
                            }
                        }
                    }
                    MessageBox.Show("SKU Update Process Completed..");
                }
                else
                {
                    MessageBox.Show("There is no Products to Process");
                }
            }
            catch (Exception ex)
            {
                string strMsg = "SQL Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            NpgsqlConn.Close();
        }


        private void UpdateSKUDetails1()
        {
            try
            {
                NpgsqlConn.Open();

                DataSet Ds = new DataSet();
                NpgsqlCommand cmd = new NpgsqlCommand("SELECT SKU.product_id, SKU.mrp_id, SKU.stock_keeping_unit_id FROM stock_keeping_unit SKU LEFT JOIN product P ON P.product_id = SKU.product_id LEFT JOIN location_product_map lpm ON lpm.product_id = P.product_id WHERE P.client_id = " + iClientID + " AND lpm.location_id = " + iLocationID + " AND P.is_active = 'true' ORDER BY P.product_id", NpgsqlConn);
                cmd.CommandTimeout = 300;

                NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
                da.Fill(Ds);
                da.Dispose();

                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtRow in Ds.Tables[0].Rows)
                    {
                        int iProductID = Convert.ToInt32(dtRow["product_id"].ToString());
                        int iMrpID = Convert.ToInt32(dtRow["mrp_id"].ToString());
                        int iSkuID = Convert.ToInt32(dtRow["stock_keeping_unit_id"].ToString());

                        DataSet Ds1 = new DataSet();
                        NpgsqlCommand cmd1 = new NpgsqlCommand("SELECT gd.product_id, gd.mrp_id, g.location_id, gd.rate FROM goods_received_note_detail gd LEFT JOIN goods_received_note g ON g.goods_received_note_id = gd.goods_received_note_id WHERE gd.product_id = " + iProductID + " AND gd.mrp_id = " + iMrpID + " AND gd.modified_on = (SELECT modified_on FROM goods_received_note_detail WHERE product_id = " + iProductID + " AND mrp_id = " + iMrpID + " order by modified_on DESC LIMIT 1) AND g.location_id = " + iLocationID + ";", NpgsqlConn);
                        cmd1.CommandTimeout = 300;

                        NpgsqlDataAdapter da1 = new NpgsqlDataAdapter(cmd1);
                        da1.Fill(Ds1);
                        da1.Dispose();

                        if (Ds1 != null && Ds1.Tables[0].Rows.Count > 0)
                        {
                            float fRate = float.Parse(Ds1.Tables[0].Rows[0]["rate"].ToString());

                            NpgsqlCommand cmd2 = new NpgsqlCommand("UPDATE location_stock_keeping_unit_map SET actual_landing_price = " + fRate + ", modified_on = now() at time zone 'utc' WHERE stock_keeping_unit_id = " + iSkuID + " AND location_id = " + iLocationID + ";", NpgsqlConn);
                            cmd2.CommandTimeout = 300;
                            cmd2.ExecuteNonQuery();
                        }
                    }
                    MessageBox.Show("SKU Update Process Completed..");
                }
                else
                {
                    MessageBox.Show("There is no Products to Process");
                }
            }
            catch (Exception ex)
            {
                string strMsg = "SQL Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            NpgsqlConn.Close();
        }

        //public void GetLocationDetails()
        //{
        //    List<Product> oPro = new List<Product>();
        //    List<Location_Details> oLoc = new List<Location_Details>();
        //    List<Stock_Keeping_Unit> oSku = new List<Stock_Keeping_Unit>();

        //    ServerMethod oSer = new ServerMethod();

        //    oLoc = oSer.GetClientLocationDetails();

        //    if(oLoc != null && oLoc.Count > 0)
        //    {
        //        for(int i = 0; i < oLoc.Count; i++)
        //        {
        //            oPro = oSer.GetLocationProductDetails(oLoc[i].location_id);

        //            if(oPro != null && oPro.Count > 0)
        //            {
        //                for(int j = 0; j < oPro.Count; j++)
        //                {
        //                    oSku = oSer.GetLocationSkuDetails(oLoc[i].location_id, oPro[j].product_id);

        //                    if(oSku != null && oSku.Count > 0)
        //                    {
        //                        for(int k = 0; k < oSku.Count; k++)
        //                        {

        //                        }
        //                    }
        //                }
        //            }
        //        }                
        //    }
        //}


    }
}
