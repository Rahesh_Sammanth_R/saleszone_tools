﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Web;
//using System.Web.Hosting;

//using System.Windows.Forms;
using System.Reflection;
using System.Xml;
//using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace SKU_Inventory_Update
{
    public class Logger
    {
        /// <summary>
        /// MsgType Property is used to indicate the type of event logging either in Log file or send an email with error information or both.
        /// </summary>
        public MessageType MsgType
        {
            get
            {
                return this.MT;
            }
            set
            {
                this.MT = value;
            }
        }
        public MessageType MT = new MessageType();

        public string LogName = string.Empty;
        public string InfoLogName = string.Empty;

        /// <summary>
        /// default constructor
        /// </summary>
        public Logger()
        {
            this.LogName = "ErrorLog";
            this.InfoLogName = "InfoLog";
        }

        /// <summary>
        /// RaiseError is called to select the operation to be done
        /// </summary>
        /// <param name="_message"> any message </param>
        /// <param name="ex"> exception  </param>
        public void RaiseError(string _message, Exception ex)
        {

            switch (this.MT)
            {
                case MessageType.EventLog:
                    SaveToEventLog(_message, ex);
                    break;
                case MessageType.MailAndEventLog:
                    SaveToEventLog(_message, ex);
                    break;
                default:
                    break;
            }
        }

        public void RaisInfo(string _message)
        {
            switch (this.MT)
            {
                case MessageType.InfoLog:
                    SaveToInfoLog(_message);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Formats and logs error
        /// </summary>
        /// <param name="_message"> any message </param>
        /// <param name="ex"> exception  </param>
        private void SaveToEventLog(string _message, Exception ex)
        {
            try
            {
                //string strPhysicalApplicationPath = HostingEnvironment.ApplicationPhysicalPath;
                //string strPhysicalApplicationPath = Application.StartupPath;
                string strPhysicalApplicationPath = Directory.GetParent(Assembly.GetExecutingAssembly().Location).ToString();

                if (GetDirectorySize(strPhysicalApplicationPath + "\\Logs\\") > 5000000)
                    DeleteAll(strPhysicalApplicationPath + "\\Logs\\");

                string strFileName = strPhysicalApplicationPath + "\\Logs\\" + LogName + DateTime.Now.ToString("ddMMMyyyy") + ".txt";
                string strBody = string.Empty;
                FileInfo fInfo = new FileInfo(strFileName);

                string _IP = string.Empty;
                System.Net.IPHostEntry _IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
                foreach (System.Net.IPAddress _IPAddress in _IPHostEntry.AddressList)
                {
                    // InterNetwork indicates that an IP version 4 address is expected 
                    // when a Socket connects to an endpoint
                    if (_IPAddress.AddressFamily.ToString() == "InterNetwork")
                    {
                        _IP = _IPAddress.ToString();
                    }
                }

                strBody = _message + Environment.NewLine + Environment.NewLine;

                strBody += "Server Address :: " + _IP + Environment.NewLine;
                strBody += "Occured At     :: " + DateTime.Now + Environment.NewLine + Environment.NewLine;
                strBody += "################################## -- Start of Error  -- #################################" + Environment.NewLine;
                strBody += ex.StackTrace + Environment.NewLine;
                strBody += "################################### -- End of Error -- ###################################" + Environment.NewLine + Environment.NewLine;


                DirectoryInfo dInfo = new DirectoryInfo(strPhysicalApplicationPath + "\\Logs\\");
                if (!dInfo.Exists)
                {
                    dInfo.Create();
                }

                if (fInfo.Exists)
                {
                    FileStream fStream = new FileStream(strFileName, FileMode.Append, FileAccess.Write);
                    StreamWriter sWriter = new StreamWriter(fStream);
                    sWriter.WriteLine(strBody);
                    sWriter.Close();
                }
                else
                {
                    FileStream fStream = new FileStream(strFileName, FileMode.Create, FileAccess.Write);
                    StreamWriter sWriter = new StreamWriter(fStream);
                    sWriter.WriteLine(strBody);
                    sWriter.Close();
                }
            }
            catch (Exception ex1)
            {
                //sendErrorMail(e.Message);
                throw (ex1);
            }
        }

        public static long GetDirectorySize(string parentDirectory)
        {
            if (Directory.Exists(parentDirectory))
                return new DirectoryInfo(parentDirectory).GetFiles("*.*", SearchOption.AllDirectories).Sum(file => file.Length);
            else
                return 0;
        }

        public void DeleteAll(string path)
        {
            string[] directories = Directory.GetDirectories(path);
            string[] files = Directory.GetFiles(path);
            foreach (string x in directories)
                Directory.Delete(x, true);
            foreach (string x in files)
                File.Delete(x);
        }


        private void SaveToInfoLog(string _message)
        {
            try
            {
                //string strPhysicalApplicationPath = HostingEnvironment.ApplicationPhysicalPath;
                string strPhysicalApplicationPath = Directory.GetParent(Assembly.GetExecutingAssembly().Location).ToString();

                if (GetDirectorySize(strPhysicalApplicationPath + "\\InfoLogs\\") > 5000000)
                    DeleteAll(strPhysicalApplicationPath + "\\InfoLogs\\");

                string strFileName = strPhysicalApplicationPath + "\\InfoLogs\\" + InfoLogName + DateTime.Now.ToString("ddMMMyyyy") + ".txt";
                string strBody = string.Empty;
                FileInfo fInfo = new FileInfo(strFileName);

                strBody = _message + Environment.NewLine;

                DirectoryInfo dInfo = new DirectoryInfo(strPhysicalApplicationPath + "\\InfoLogs\\");
                if (!dInfo.Exists)
                {
                    dInfo.Create();
                }

                if (fInfo.Exists)
                {
                    FileStream fStream = new FileStream(strFileName, FileMode.Append, FileAccess.Write);
                    StreamWriter sWriter = new StreamWriter(fStream);
                    sWriter.WriteLine(strBody);
                    sWriter.Close();
                }
                else
                {
                    FileStream fStream = new FileStream(strFileName, FileMode.Create, FileAccess.Write);
                    StreamWriter sWriter = new StreamWriter(fStream);
                    sWriter.WriteLine(strBody);
                    sWriter.Close();
                }
            }
            catch (Exception)
            {
                //sendErrorMail(e.Message);
            }
        }
    }
    /// <summary>
    /// Enum MessageType is accessed to provide the operation to be done
    /// </summary>
    public enum MessageType
    {
        EventLog,
        InfoLog,
        SendMail,
        MailAndEventLog,
        BillLog
    }
}
