﻿using System;
using System.Collections.Generic;
using System.Text;

using SKU_Inventory_Update.Entity;
using System.Configuration;

namespace SKU_Inventory_Update.Helper
{
    public class ServerMethod
    {
        string MasterURL = ConfigurationManager.AppSettings["MasterURL"].ToString();
        string ClientID = ConfigurationManager.AppSettings["ClientID"].ToString();
        string AppToken = ConfigurationManager.AppSettings["AppToken"].ToString();

        public List<Location_Details> GetClientLocationDetails()
        {
            List<Location_Details> model = new List<Location_Details>();
            try
            {
                var RequestURL = MasterURL + "/" + "GetClientLocationDetails" + "/" + ClientID + "/" + AppToken;

                string tObj = ServerInteractor.CallWebServiceSync(RequestURL);

                model = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Location_Details>>(tObj, JSONDateFormatter());

                if (model != null && model.Count > 0)
                {
                    return model;
                }
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            return null;
        }


        public List<Product> GetLocationProductDetails(int iLocID)
        {
            List<Product> oPro = new List<Product>();
            try
            {
                var RequestURL = MasterURL + "/" + "GetLocationProductsInfo" + "/" + ClientID + "/" + iLocID.ToString() + "/" + AppToken;

                string tObj = ServerInteractor.CallWebServiceSync(RequestURL);

                oPro = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Product>>(tObj, JSONDateFormatter());

                if (oPro != null && oPro.Count > 0)
                {
                    return oPro;
                }
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            return null;
        }


        public List<Stock_Keeping_Unit> GetLocationSkuDetails(int iLocID, int iProdID)
        {
            List<Stock_Keeping_Unit> oSku = new List<Stock_Keeping_Unit>();
            try
            {
                var RequestURL = MasterURL + "/" + "GetSKUDetailsForProduct_1" + "/" + iLocID.ToString() + "/" + ClientID + "/" + iProdID.ToString() + "/" + AppToken;

                string tObj = ServerInteractor.CallWebServiceSync(RequestURL);

                oSku = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Stock_Keeping_Unit>>(tObj, JSONDateFormatter());

                if (oSku != null && oSku.Count > 0)
                {
                    return oSku;
                }
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            return null;
        }


        public static Newtonsoft.Json.Converters.IsoDateTimeConverter JSONDateFormatter()
        {
            var format = "dd-MM-yyyy HH:mm:ss"; // your datetime format
            var dateTimeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter { DateTimeFormat = format };
            return dateTimeConverter;
        }

    }
}
