﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using Newtonsoft.Json;

using Plugin.Connectivity;

namespace SKU_Inventory_Update
{
    public class ServerInteractor
    {
        public static async Task<string> CallWebService(string strRequest)
        {
            using (HttpClient client = new HttpClient())
            {
                var responseMsg = await client.GetAsync(strRequest);
                responseMsg.EnsureSuccessStatusCode();
                string responseBody = await responseMsg.Content.ReadAsStringAsync();
                //return JsonConvert.DeserializeObject(responseBody);
                return responseBody;
            }
        }

        public static string CallWebServiceSync(string strRequest)
        {
            using (HttpClient client = new HttpClient())
            {
                var responseMsg = client.GetAsync(strRequest).Result;

                if (responseMsg.IsSuccessStatusCode)
                {
                    // by calling .Result you are performing a synchronous call
                    var responseContent = responseMsg.Content;

                    // by calling .Result you are synchronously reading the result
                    string responseBody = responseContent.ReadAsStringAsync().Result;
                    return responseBody;
                }
                return string.Empty;
            }
        }


        public static async Task<string> CallPostService(string Uri, object oRequest)
        {
            using (HttpClient client = new HttpClient())
            {
                //var responseMsg = await client.PostAsync(Uri, new StringContent(JsonConvert.SerializeObject(oRequest).ToString(), Encoding.UTF8, "application/json")).Result;

                /* client.Timeout = TimeSpan.FromSeconds(Convert.ToDouble(10)); --Rahesh */
                var content = new StringContent(oRequest.ToString(), Encoding.UTF8, "application/json");
                var result = await client.PostAsync(Uri, content).ConfigureAwait(false);
                result.EnsureSuccessStatusCode();
                string responseBody = await result.Content.ReadAsStringAsync();
                return responseBody;
                ////Set2
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create("path/api");
                //httpWebRequest.ContentType = "text/json";
                //httpWebRequest.Method = "POST";
                //using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
                //{
                //    streamWriter.Write(oRequest);
                //    streamWriter.Flush();
                //}
                //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                //responseMsg.EnsureSuccessStatusCode();
                //string responseBody = await responseMsg.Content.ReadAsStringAsync();
                ////return JsonConvert.DeserializeObject(responseBody);
                //return responseBody;

                //var content = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
                //var result = client.PostAsync(url, content).Result;
            }

            return string.Empty;
        }

        public static async Task<string> CallInvNoPostService(string Uri, object oRequest)
        {
            using (HttpClient Hclient = new HttpClient())
            {
                //var responseMsg = await client.PostAsync(Uri, new StringContent(JsonConvert.SerializeObject(oRequest).ToString(), Encoding.UTF8, "application/json")).Result;

                Hclient.Timeout = TimeSpan.FromSeconds(Convert.ToDouble(10));
                var content = new StringContent(oRequest.ToString(), Encoding.UTF8, "application/json");
                var result = await Hclient.PostAsync(Uri, content).ConfigureAwait(false);
                result.EnsureSuccessStatusCode();
                string responseBody = await result.Content.ReadAsStringAsync();
                return responseBody;
            }

            return string.Empty;
        }

        public static async Task<string> CallCustInfoPostService(string Uri, object oRequest)
        {
            using (HttpClient Hclient = new HttpClient())
            {
                //var responseMsg = await client.PostAsync(Uri, new StringContent(JsonConvert.SerializeObject(oRequest).ToString(), Encoding.UTF8, "application/json")).Result;

                Hclient.Timeout = TimeSpan.FromSeconds(Convert.ToDouble(60));
                var content = new StringContent(oRequest.ToString(), Encoding.UTF8, "application/json");
                var result = await Hclient.PostAsync(Uri, content).ConfigureAwait(false);
                result.EnsureSuccessStatusCode();
                string responseBody = await result.Content.ReadAsStringAsync();
                return responseBody;
            }

            return string.Empty;
        }


        public static async Task<string> CallTransferNoteSave(string Uri, object oRequest)
        {
            using (HttpClient Hclient = new HttpClient())
            {
                //Hclient.Timeout = TimeSpan.FromSeconds(Convert.ToDouble(10));
                var content = new StringContent(oRequest.ToString(), Encoding.UTF8, "application/json");
                var result = await Hclient.PostAsync(Uri, content).ConfigureAwait(false);
                result.EnsureSuccessStatusCode();
                string responseBody = await result.Content.ReadAsStringAsync();
                return responseBody;
            }
            return string.Empty;
        }

        //public static int NetworkAvailability()
        //{
        //    if (!NetworkInterface.GetIsNetworkAvailable())
        //        return (int)NETWORK_STATUS.NETWORK_NOT_AVAILABLE;

        //    return 0;
        //}


        public static async Task<int> NetworkAvailability()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
                return (int)NETWORK_STATUS.NETWORK_NOT_AVAILABLE;

            return 0;
        }

        public static async Task<bool> CheckConnectivity()
        {
            bool canReach = await CrossConnectivity.Current.IsRemoteReachable("google.com");
            return canReach;
        }

    }

    public enum NETWORK_STATUS
    {
        INTERNET_AVAILABLE = 0,
        NETWORK_NOT_AVAILABLE = 1,
        INTERNET_NOT_AVAILABLE = 2
    }
}
