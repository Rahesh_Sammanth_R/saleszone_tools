﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace SKU_Inventory_Update.Entity
{
    public class Location_Details
    {
        public int location_id { get; set; }

        public string location_name { get; set; }

        public string location_type { get; set; }

        public string address { get; set; }

        public string contact_no { get; set; }

        public int state_id { get; set; }

        public string state { get; set; }

        public string country { get; set; }

        public int adminuser { get; set; }

        public int billing_group_id { get; set; }

        public string billing_group_name { get; set; }

        public string gst_no { get; set; }

        public DateTime modified_on { get; set; }

        public float profit_percentage { get; set; }
    }
}
