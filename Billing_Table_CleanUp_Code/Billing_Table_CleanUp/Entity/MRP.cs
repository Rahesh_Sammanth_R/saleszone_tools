﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Table_CleanUp.Entity
{
    public class MRP
    {
        public int mrp_id { get; set; }

        public int product_id { get; set; }

        public float mrp_price { get; set; }

        public float sales_price { get; set; }

        public DateTime modified_on { get; set; }

        public bool is_active { get; set; }
    }
}
