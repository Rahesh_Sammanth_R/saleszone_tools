﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Table_CleanUp.Entity
{
    public class Stock_Keeping_Unit
    {
        public int stock_keeping_unit_id { get; set; }

        public DateTime modified_on { get; set; }

        public string stock_keeping_unit_no { get; set; }

        public int product_id { get; set; }

        public int variance_id { get; set; }

        public int mrp_id { get; set; }

        public float landing_price { get; set; }

        public float avg_landing_price { get; set; }

        public bool is_server_synced { get; set; }
    }
}
