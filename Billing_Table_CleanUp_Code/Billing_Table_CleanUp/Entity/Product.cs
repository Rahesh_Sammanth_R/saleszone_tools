﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SQLite;
using SQLite.Net;
using SQLite.Net.Attributes;

namespace Billing_Table_CleanUp.Entity
{
    public class Product
    {
        //[PrimaryKey]
        //[NotNull]
        //[Indexed(Name = "prod_id_unique", Order = 1, Unique = true)]
        public int product_id { get; set; }

        public string product_name { get; set; }

        public string product_desc { get; set; }

        public string product_print_label { get; set; }

        public string product_img { get; set; }

        //[Indexed(Name = "prod_modi", Order = 1)]
        public DateTime modified_on { get; set; }

        public float gst { get; set; }

        public float s_gst { get; set; }

        public float c_gst { get; set; }

        public float i_gst { get; set; }

        public float ut_gst { get; set; }

        public string hsn_code { get; set; }

        public int category_id { get; set; }

        public string category_name { get; set; }

        public int brand_id { get; set; }

        public string brand_name { get; set; }

        public int pack_size_id { get; set; }

        public string pack_size_name { get; set; }

        public float standard_brand_discount { get; set; }

        public int unit_id { get; set; }

        public string unit_name { get; set; }

        public string unit_short_code { get; set; }

        public bool is_active { get; set; }

        public int status { get; set; }

        public float current_stock { get; set; }

        public bool reward_points_applicable { get; set; }

        public bool apply_credit_card_charges { get; set; }

        public bool meal_pass_allowed { get; set; }

        public string margin_type { get; set; }

        public float cess { get; set; }
    }
}
