﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Table_CleanUp.Entity
{
    public class Customer_Group_Product_Map
    {
        public int customer_group_id { get; set; }

        public int product_id { get; set; }

        public string margin_type { get; set; }

        public string value_mode { get; set; }

        public float product_or_margin_value { get; set; }

        public DateTime modified_on { get; set; }
    }
}
