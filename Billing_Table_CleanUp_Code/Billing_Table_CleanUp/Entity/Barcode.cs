﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Table_CleanUp.Entity
{
    public class Barcode
    {
        public int barcode_id { get; set; }

        public string barcode { get; set; }

        public decimal sales_price { get; set; }

        public decimal mrp { get; set; }

        public DateTime modified_on { get; set; }

        public int product_id { get; set; }

        public bool is_active { get; set; }
    }
}
