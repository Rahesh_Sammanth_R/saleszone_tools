﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Table_CleanUp.DB
{
    public class ConnectionGateKeeper
    {
        #region Private Members

        public object LockObject { get; set; }

        #endregion Private Members


        #region public Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ConnectionGateKeeper()
        {
            LockObject = new object();
        }

        #endregion public Constructor
    }
}
