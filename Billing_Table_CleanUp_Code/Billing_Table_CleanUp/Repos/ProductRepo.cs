﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SQLite;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Interop;
using Billing_Table_CleanUp.DB;
using Billing_Table_CleanUp.Entity;

namespace Billing_Table_CleanUp.Repos
{
    public class ProductRepo : IDisposable
    {
        SQLiteConnection _connection;
        ConnectionGateKeeper oConnectionGateKeeper;

        public void Dispose()
        {
            _connection.Dispose();
        }

        public ProductRepo(string path, ISQLitePlatform sqlitePlatform)
        {
            //_connection = new SQLiteConnection(sqlitePlatform, path, false);
            _connection = new SQLiteConnection(sqlitePlatform, path, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex, false);
            _connection.BusyTimeout = new TimeSpan(0, 0, 30);
            //oConnectionGateKeeper = ConnectionGateKeeper.GateKeeperInstance();
            oConnectionGateKeeper = TableGateKeeper.GetTableGateKeeper<Product>();
        }

        public ProductRepo(SQLite.Net.SQLiteConnection Conn)
        {
            _connection = Conn;
            oConnectionGateKeeper = TableGateKeeper.GetTableGateKeeper<Product>();
        }

        public List<Product> GetAllProducts()
        {
            return _connection.Table<Product>().Where(t => t.is_active.Equals(true)).ToList();
        }

        public int AddOrUpdatePro(Product oPro)
        {
            if (oPro.product_id != 0)
                lock (oConnectionGateKeeper.LockObject)
                    return _connection.InsertOrReplace(oPro);
            else
                return 0;
        }

        public List<Product> BulkAddOrUpdatePro(List<Product> oPros)
        {
            List<Product> insertPro = new List<Product>();
            List<Product> updatePro = new List<Product>();
            if (oPros.Count > 0)
            {
                lock (oConnectionGateKeeper.LockObject)
                {
                    foreach (Product oPro in oPros)
                    {
                        if (_connection.Table<Product>().Where(t => t.product_id.Equals(oPro.product_id)).Count() > 0)
                            updatePro.Add(oPro);
                        else
                            insertPro.Add(oPro);
                    }

                    _connection.InsertAll(insertPro);
                    _connection.InsertOrReplaceAll(updatePro);
                }
            }

            return insertPro;
        }


    }
}
