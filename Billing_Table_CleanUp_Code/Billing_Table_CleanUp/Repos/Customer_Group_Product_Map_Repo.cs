﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SQLite;
using SQLite.Net;
using SQLite.Net.Interop;
using Billing_Table_CleanUp.DB;
using Billing_Table_CleanUp.Entity;

namespace Billing_Table_CleanUp.Repos
{
    public class Customer_Group_Product_Map_Repo : IDisposable
    {
        SQLiteConnection _connection;
        ConnectionGateKeeper oConnectionGateKeeper;

        public void Dispose()
        {
            _connection.Dispose();
        }

        public Customer_Group_Product_Map_Repo(string path, ISQLitePlatform sqlitePlatform)
        {
            //_connection = new SQLiteConnection(sqlitePlatform, path, false);
            _connection = new SQLiteConnection(sqlitePlatform, path, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex, false);
            _connection.BusyTimeout = new TimeSpan(0, 0, 30);
            //    oConnectionGateKeeper = ConnectionGateKeeper.GateKeeperInstance();
            oConnectionGateKeeper = TableGateKeeper.GetTableGateKeeper<Customer_Group_Product_Map>();
        }


        public Customer_Group_Product_Map_Repo(SQLite.Net.SQLiteConnection Conn)
        {
            _connection = Conn;
            //oConnectionGateKeeper = ConnectionGateKeeper.GateKeeperInstance();
            oConnectionGateKeeper = TableGateKeeper.GetTableGateKeeper<Customer_Group_Product_Map>();
        }

        //public int BulkAddOrUpdateCustGrp(List<Customer_Group_Product_Map> oCustGrps)
        //{
        //    if (oCustGrps.Count > 0)
        //    {
        //        lock (oConnectionGateKeeper.LockObject)
        //        {
        //            foreach (Customer_Group_Product_Map oCustGrp in oCustGrps)
        //            {
        //                if (oCustGrp.customer_group_id != 0)
        //                {
        //                    oCustGrp.modified_on = DateTime.UtcNow;
        //                    _connection.InsertOrReplace(oCustGrp);
        //                }
        //            }
        //        }
        //        return 1;
        //    }
        //    else
        //        return 0;
        //}



        public int BulkAddOrUpdateCustGrp(List<Customer_Group_Product_Map> oCustGrps)
        {
            try
            {
                if (oCustGrps.Count > 0)
                {
                    lock (oConnectionGateKeeper.LockObject)
                    {
                        List<Customer_Group_Product_Map> insertCustGrp = new List<Customer_Group_Product_Map>();
                        List<Customer_Group_Product_Map> updateCustGrp = new List<Customer_Group_Product_Map>();

                        foreach (Customer_Group_Product_Map oCustGrp in oCustGrps)
                        {
                            if (oCustGrp.customer_group_id == 0 || oCustGrp.product_id == 0)
                                continue;

                            if (_connection.Table<Customer_Group_Product_Map>().Where(t => (t.product_id.Equals(oCustGrp.product_id)) && (t.customer_group_id.Equals(oCustGrp.customer_group_id))).Count() > 0)
                                updateCustGrp.Add(oCustGrp);
                            else
                                insertCustGrp.Add(oCustGrp);
                        }

                        _connection.InsertAll(insertCustGrp);
                        _connection.InsertOrReplaceAll(updateCustGrp);
                    }
                    return 1;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {

            }
            return 0;
        }

        public List<Customer_Group_Product_Map> GetCustomerGroupProductMap()
        {
            return _connection.Table<Customer_Group_Product_Map>().ToList();
        }
    }
}
