﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SQLite;
using SQLite.Net;
using SQLite.Net.Interop;
using Billing_Table_CleanUp.DB;
using Billing_Table_CleanUp.Entity;

namespace Billing_Table_CleanUp.Repos
{
    public class SKURepo : IDisposable
    {
        SQLiteConnection _connection;
        ConnectionGateKeeper oConnectionGateKeeper;

        public void Dispose()
        {
            _connection.Dispose();
        }

        public SKURepo(string path, ISQLitePlatform sqlitePlatform)
        {
            //_connection = new SQLiteConnection(sqlitePlatform, path, false);
            _connection = new SQLiteConnection(sqlitePlatform, path, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex, false);
            _connection.BusyTimeout = new TimeSpan(0, 0, 30);
            //oConnectionGateKeeper = ConnectionGateKeeper.GateKeeperInstance();
            oConnectionGateKeeper = TableGateKeeper.GetTableGateKeeper<Stock_Keeping_Unit>();
        }

        public SKURepo(SQLite.Net.SQLiteConnection Conn)
        {
            _connection = Conn;
            //oConnectionGateKeeper = ConnectionGateKeeper.GateKeeperInstance();
            oConnectionGateKeeper = TableGateKeeper.GetTableGateKeeper<Stock_Keeping_Unit>();
        }

        public int AddOrUpdateSKU(Stock_Keeping_Unit oSku)
        {
            if (oSku.stock_keeping_unit_id != 0)
                lock (oConnectionGateKeeper.LockObject)
                    return _connection.InsertOrReplace(oSku);
            else
                return 0;
        }

        public int BulkAddOrUpdateSKU(List<Stock_Keeping_Unit> oSKU)
        {
            try
            {
                if (oSKU.Count > 0)
                {
                    lock (oConnectionGateKeeper.LockObject)
                    {
                        List<Stock_Keeping_Unit> insertSku = new List<Stock_Keeping_Unit>();
                        List<Stock_Keeping_Unit> updateSku = new List<Stock_Keeping_Unit>();

                        foreach (Stock_Keeping_Unit sku in oSKU)
                        {
                            sku.is_server_synced = true;
                            if (_connection.Table<Stock_Keeping_Unit>().Where(t => t.stock_keeping_unit_id.Equals(sku.stock_keeping_unit_id)).Count() > 0)
                                updateSku.Add(sku);
                            else
                                insertSku.Add(sku);
                        }


                        _connection.InsertAll(insertSku);
                        _connection.InsertOrReplaceAll(updateSku);
                    }
                    return 1;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {

            }
            return 0;
        }

        public List<Stock_Keeping_Unit> GetSKU()
        {
            if (_connection.Table<Stock_Keeping_Unit>().Count() > 0)
                return _connection.Table<Stock_Keeping_Unit>().ToList();

            return null;
        }

    }
}
