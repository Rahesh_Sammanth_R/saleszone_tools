﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SQLite;
using SQLite.Net;
using SQLite.Net.Interop;
using Billing_Table_CleanUp.DB;
using Billing_Table_CleanUp.Entity;

namespace Billing_Table_CleanUp.Repos
{
    public class MRPRepo : IDisposable
    {
        SQLiteConnection _connection;
        ConnectionGateKeeper oConnectionGateKeeper;

        public void Dispose()
        {
            _connection.Dispose();
        }

        public MRPRepo(string path, ISQLitePlatform sqlitePlatform)
        {
            //_connection = new SQLiteConnection(sqlitePlatform, path, false);
            _connection = new SQLiteConnection(sqlitePlatform, path, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex, false);
            _connection.BusyTimeout = new TimeSpan(0, 0, 30);
            //oConnectionGateKeeper = ConnectionGateKeeper.GateKeeperInstance();
            oConnectionGateKeeper = TableGateKeeper.GetTableGateKeeper<MRP>();
        }

        public MRPRepo(SQLite.Net.SQLiteConnection Conn)
        {
            _connection = Conn;
            //oConnectionGateKeeper = ConnectionGateKeeper.GateKeeperInstance();
            oConnectionGateKeeper = TableGateKeeper.GetTableGateKeeper<MRP>();
        }

        public int AddOrUpdateMrp(MRP oMRP)
        {
            if (oMRP.product_id != 0)
            {
                lock (oConnectionGateKeeper.LockObject)
                    return _connection.InsertOrReplace(oMRP);
            }
            else
                return 0;
        }


        //public int BulkAddOrUpdateMRP(List<MRP> oMRPs)
        //{
        //    if (oMRPs.Count > 0)
        //    {
        //        lock (oConnectionGateKeeper.LockObject)
        //        {
        //            foreach (MRP oMRP in oMRPs)
        //            {
        //                if (oMRP.mrp_id != 0)
        //                {
        //                    oMRP.modified_on = DateTime.UtcNow;
        //                    _connection.InsertOrReplace(oMRP);
        //                }
        //            }
        //        }
        //        return 1;
        //    }
        //    else
        //        return 0;
        //}

        public List<MRP> BulkAddOrUpdateMRP(List<MRP> oMRPs)
        {
            List<MRP> insertMRP = new List<MRP>();
            List<MRP> updateMRP = new List<MRP>();

            if (oMRPs.Count > 0)
            {
                lock (oConnectionGateKeeper.LockObject)
                {  
                    foreach (MRP oPro in oMRPs)
                    {
                        //oPro.modified_on = DateTime.UtcNow;
                        if (_connection.Table<MRP>().Where(t => t.mrp_id.Equals(oPro.mrp_id)).Count() > 0)
                            updateMRP.Add(oPro);
                        else
                            insertMRP.Add(oPro);
                    }

                    _connection.InsertAll(insertMRP);
                    _connection.InsertOrReplaceAll(updateMRP);
                }
            }
            return insertMRP;
        }

        public List<MRP> GetMRP()
        {
                if (_connection.Table<MRP>().Count() > 0)
                    return _connection.Table<MRP>().Where(t => t.is_active.Equals(true)).ToList();

            return null;
        }


    }
}
