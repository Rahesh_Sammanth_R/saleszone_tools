﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SQLite;
using SQLite.Net;
using SQLite.Net.Interop;
using Billing_Table_CleanUp.DB;
using Billing_Table_CleanUp.Entity;

namespace Billing_Table_CleanUp.Repos
{
    public class BarcodeRepo : IDisposable
    {
        SQLiteConnection _connection;

        ConnectionGateKeeper oGate;

        public void Dispose()
        {
            _connection.Dispose();
        }

        public BarcodeRepo(string path, ISQLitePlatform sqlitePlatform)
        {
            _connection = new SQLiteConnection(sqlitePlatform, path, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex, false);
            _connection.BusyTimeout = new TimeSpan(0, 0, 30);
            //oGate = ConnectionGateKeeper.GateKeeperInstance();
            oGate = TableGateKeeper.GetTableGateKeeper<Barcode>();
        }

        public BarcodeRepo(SQLite.Net.SQLiteConnection Conn)
        {
            _connection = Conn;
            oGate = TableGateKeeper.GetTableGateKeeper<Barcode>();
        }

        public int AddOrUpdateBarcode(Barcode oPro)
        {

            if (oPro.product_id != 0)
            {
                lock (oGate.LockObject)
                {
                    return _connection.InsertOrReplace(oPro);
                }
            }
            else
                return 0;
        }

        public List<Barcode> BulkAddOrUpdateBar(List<Barcode> oBars)
        {
            List<Barcode> insertBar = new List<Barcode>();
            List<Barcode> updateBar = new List<Barcode>();

            if (oBars.Count > 0)
            {
                lock (oGate.LockObject)
                {
                    foreach (Barcode oBar in oBars)
                    {
                        if (_connection.Table<Barcode>().Where(t => t.barcode_id.Equals(oBar.barcode_id)).Count() > 0)
                            updateBar.Add(oBar);
                        else
                            insertBar.Add(oBar);
                    }
                    _connection.InsertAll(insertBar);
                    _connection.InsertOrReplaceAll(updateBar);
                }
            }
            return insertBar;
        }

        public List<Barcode> GetBarcodes()
        {
                if (_connection.Table<Barcode>().Count() > 0)
                    return _connection.Table<Barcode>().Where(t => t.is_active.Equals(true)).ToList();

            return null;
        }

    }
}
