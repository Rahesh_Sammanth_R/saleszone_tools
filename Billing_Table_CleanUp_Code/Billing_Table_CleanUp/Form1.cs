﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SQLite.Net.Interop;
using System.IO;
using System.Reflection;
using System.Data.SQLite;
using Billing_Table_CleanUp.Entity;
using Billing_Table_CleanUp.Repos;
using Billing_Table_CleanUp.Helper;


namespace Billing_Table_CleanUp
{
    public partial class Form1 : Form
    {
        List<Product> oSupportProductList = new List<Product>();
        List<Barcode> oSupportBarcodeList = new List<Barcode>();
        List<MRP> oSupportMRPList = new List<MRP>();
        List<Customer_Group_Product_Map> oSupportCGPMList = new List<Customer_Group_Product_Map>();
        List<Stock_Keeping_Unit> oSupportSKUList = new List<Stock_Keeping_Unit>();


        List<Product> oSourceProductList = new List<Product>();
        List<Barcode> oSourceBarcodeList = new List<Barcode>();
        List<MRP> oSourceMRPList = new List<MRP>();
        List<Customer_Group_Product_Map> oSourceCGPMList = new List<Customer_Group_Product_Map>();
        List<Stock_Keeping_Unit> oSourceSKUList = new List<Stock_Keeping_Unit>();

        Logger objInfoHandler = new Logger();        

        public Form1()
        {
            InitializeComponent();
            objInfoHandler.MsgType = MessageType.InfoLog;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            try
            {
                
                objInfoHandler.RaisInfo("Table Clean-Up Process Started " + DateTime.Now + "");

                DeleteSourceDetails();
                GetTableDetails();
                InsertTableDetails();

                objInfoHandler.RaisInfo("Table Clean-Up Process Ended " + DateTime.Now + "");
                MessageBox.Show("Table CleanUp Process Completed...");
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //public static string GetSourceDbPathAsync()
        //{
        //    //var folder = System.AppDomain.CurrentDomain.BaseDirectory;
        //    return System.AppDomain.CurrentDomain.BaseDirectory + "Todo.db3";
        //}

        private static ISQLitePlatform GetSqlitePlatform()
        {
            return new SQLite.Net.Platform.Generic.SQLitePlatformGeneric();
        }


        private static string GetSupportDbPath()
        {
            try
            {
                //string strDbPath = "";
                //string strPhysicalApplicationPath = Directory.GetParent(Assembly.GetExecutingAssembly().Location).ToString();
                //string AppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                //DirectoryInfo dInfo = new DirectoryInfo(strPhysicalApplicationPath + "\\Table_CleanUp\\");
                //strDbPath = dInfo.ToString() + "Todo.db3";
                //string dbpath = strPhysicalApplicationPath + "\\Todo.db3";
                //return dbpath;

                string strDBPath = System.AppDomain.CurrentDomain.BaseDirectory + "Todo.db3";
                return strDBPath;
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            return null;
        }

        private static string GetSourceDbPath()
        {
            try
            {
                //string strDbPath = "";
                //string strPhysicalApplicationPath = Directory.GetParent(Assembly.GetExecutingAssembly().Location).ToString();
                //string AppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                //DirectoryInfo dInfo = new DirectoryInfo(strPhysicalApplicationPath);
                //strDbPath = dInfo.Parent.FullName;
                //string dbpath = strPhysicalApplicationPath + "\\Todo.db3";

                String licenseKeyPath = AppDomain.CurrentDomain.BaseDirectory; 
                DirectoryInfo baseDirInfo = new DirectoryInfo(licenseKeyPath); 
                String oneStepBackwardInPath = baseDirInfo.Parent.FullName + "\\Todo.db3";
                return oneStepBackwardInPath;
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            return null;
        }

        private void GetTableDetails()
        {
            //string SupportDBInfo = GetSupportDbPath();
            //string strdataSupport = "Data Source = " + SupportDBInfo + "; Version = 3;";
            //SQLiteConnection sqlite_support_conn = new SQLiteConnection(strdataSupport);
            try
            {
                //sqlite_support_conn.Open();

                objInfoHandler.RaisInfo("Support Table Details Fetch Start...");

                GetProductDetails();

                GetMRPDetails();

                GetBarcodeDetails();

                GetCGPMDetails();

                GetSKUDetails();

                objInfoHandler.RaisInfo("Support Table Details Fetch End...");
            }
            catch(Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            finally
            {
                //sqlite_support_conn.Close();
            }            
        }

        private void DeleteSourceDetails()
        {
            objInfoHandler.RaisInfo("Source Table Delete Method Start...");

            string SourceDBInfo = GetSourceDbPath();
            string strdataSource = "Data Source = " + SourceDBInfo + "; Version = 3;";
            SQLiteConnection sqlite_Source_conn = new SQLiteConnection(strdataSource);
            try
            {
                sqlite_Source_conn.Open();
                SQLiteCommand SqliteCmd = new SQLiteCommand("DELETE FROM product; DELETE FROM mrp; DELETE FROM barcode; DELETE FROM stock_keeping_unit; DELETE FROM customer_group_product_map;", sqlite_Source_conn);
                SqliteCmd.CommandTimeout = 100;
                SqliteCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
            sqlite_Source_conn.Close();
            objInfoHandler.RaisInfo("Source Table Delete Method End...");
        }

        private void InsertTableDetails()
        {
            try
            {
                objInfoHandler.RaisInfo("Source Table Insert Method Start...");

                InsertProduct();

                InsertMRP();

                InsertBarcode();

                InsertCGPM();

                InsertSku();

                objInfoHandler.RaisInfo("Source Table Insert Method End...");
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }


        //public void UpdateTableDetails()
        //{
        //    string SourceDBInfo = GetSourceDbPath();
        //    string strdataSource = "Data Source = " + SourceDBInfo + "; Version = 3;";
        //    SQLiteConnection sqlite_Source_conn = new SQLiteConnection(strdataSource);
        //    try
        //    {
        //        sqlite_Source_conn.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
        //        Logger objErrorHandler = new Logger();
        //        objErrorHandler.MsgType = MessageType.MailAndEventLog;
        //        objErrorHandler.RaiseError(strMsg, ex);
        //    }
        //}

        private void GetProductDetails()
        {
            try
            { 
                //SQLiteCommand SqliteCmd = new SQLiteCommand("SELECT * FROM product;", sqlite_con);
                //SqliteCmd.CommandTimeout = 100;

                //DataSet ds = new DataSet();
                //SQLiteDataAdapter da = new SQLiteDataAdapter(SqliteCmd);
                //da.Fill(ds);
                //da.Dispose();
                

                //if(ds != null && ds.Tables[0].Rows.Count > 0)
                //{
                //    var tempProdList = ds.Tables[0].AsEnumerable()
                //                  .Select(dataRow => new Product
                //                  {
                //                      product_id = dataRow.Field<int>("product_id"),
                //                      product_name = dataRow.Field<string>("product_name"),
                //                      product_desc = dataRow.Field<string>("product_desc"),
                //                      product_print_label = dataRow.Field<string>("product_print_label"),
                //                      product_img = dataRow.Field<string>("product_img"),
                //                      modified_on = dataRow.Field<DateTime>("modified_on"),
                //                      gst = dataRow.Field<float>("gst"),
                //                      s_gst = dataRow.Field<float>("s_gst"),
                //                      c_gst = dataRow.Field<float>("c_gst"),
                //                      i_gst = dataRow.Field<float>("i_gst"),
                //                      ut_gst = dataRow.Field<float>("ut_gst"),
                //                      hsn_code = dataRow.Field<string>("hsn_code"),
                //                      category_id = dataRow.Field<int>("category_id"),
                //                      category_name = dataRow.Field<string>("category_name"),
                //                      brand_id = dataRow.Field<int>("brand_id"),
                //                      brand_name = dataRow.Field<string>("brand_name"),
                //                      pack_size_id = dataRow.Field<int>("pack_size_id"),
                //                      pack_size_name = dataRow.Field<string>("pack_size_name"),
                //                      standard_brand_discount = dataRow.Field<float>("standard_brand_discount"),
                //                      unit_id = dataRow.Field<int>("unit_id"),
                //                      unit_name = dataRow.Field<string>("unit_name"),
                //                      unit_short_code = dataRow.Field<string>("unit_short_code"),
                //                      is_active = dataRow.Field<bool>("is_active"),
                //                      status = dataRow.Field<int>("status"),
                //                      current_stock = dataRow.Field<float>("current_stock"),
                //                      reward_points_applicable = dataRow.Field<bool>("reward_points_applicable"),
                //                      apply_credit_card_charges = dataRow.Field<bool>("apply_credit_card_charges"),
                //                      meal_pass_allowed = dataRow.Field<bool>("meal_pass_allowed"),
                //                      margin_type = dataRow.Field<string>("margin_type"),
                //                      cess = dataRow.Field<float>("cess")
                //                  }).ToList();

                //    oSupportProductList = tempProdList;
                //}

                using (ProductRepo oProdRepo = new ProductRepo(GetSupportDbPath(), GetSqlitePlatform()))
                {
                    objInfoHandler.RaisInfo("Product Get Start...");
                    oSupportProductList = oProdRepo.GetAllProducts();
                    objInfoHandler.RaisInfo("Product Get End...");
                }

                ////sqlite_conn
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }


        private void GetMRPDetails()
        {
            try
            {
                //SQLiteCommand SqliteCmd = new SQLiteCommand("SELECT * FROM mrp;", sqlite_con);
                //SqliteCmd.CommandTimeout = 100;

                //DataSet ds = new DataSet();
                //SQLiteDataAdapter da = new SQLiteDataAdapter(SqliteCmd);
                //da.Fill(ds);
                //da.Dispose();

                //if (ds != null && ds.Tables[0].Rows.Count > 0)
                //{
                //    var tempMrpList = ds.Tables[0].AsEnumerable()
                //                 .Select(dataRow => new MRP
                //                 {
                //                     mrp_id = dataRow.Field<int>("mrp_id"),
                //                     product_id = dataRow.Field<int>("product_id"),
                //                     mrp_price = dataRow.Field<float>("mrp_price"),
                //                     sales_price = dataRow.Field<float>("sales_price"),
                //                     modified_on = dataRow.Field<DateTime>("modified_on"),
                //                     is_active = dataRow.Field<bool>("is_active")
                //                 }).ToList();

                //    oSupportMRPList = tempMrpList;
                //}

                using (MRPRepo oMRPRepo = new MRPRepo(GetSupportDbPath(), GetSqlitePlatform()))
                {
                    objInfoHandler.RaisInfo("MRP Get Start...");
                    oSupportMRPList = oMRPRepo.GetMRP();
                    objInfoHandler.RaisInfo("MRP Get End...");
                }
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }


        private void GetBarcodeDetails()
        {
            try
            {
                //SQLiteCommand SqliteCmd = new SQLiteCommand("SELECT * FROM barcode;", sqlite_con);
                //SqliteCmd.CommandTimeout = 100;

                //DataSet ds = new DataSet();
                //SQLiteDataAdapter da = new SQLiteDataAdapter(SqliteCmd);
                //da.Fill(ds);
                //da.Dispose();


                //if (ds != null && ds.Tables[0].Rows.Count > 0)
                //{
                //    var tempBarcodeList = ds.Tables[0].AsEnumerable()
                //                 .Select(dataRow => new Barcode
                //                 {
                //                     barcode_id = dataRow.Field<int>("barcode_id"),
                //                     barcode = dataRow.Field<string>("barcode"),
                //                     sales_price = dataRow.Field<decimal>("sales_price"),
                //                     mrp = dataRow.Field<decimal>("mrp"),
                //                     modified_on = dataRow.Field<DateTime>("modified_on"),
                //                     product_id = dataRow.Field<int>("product_id"),
                //                     is_active = dataRow.Field<bool>("is_active")
                //                 }).ToList();

                //    oSupportBarcodeList = tempBarcodeList;
                //}

                using (BarcodeRepo oBarcodeRepo = new BarcodeRepo(GetSupportDbPath(), GetSqlitePlatform()))
                {
                    objInfoHandler.RaisInfo("Barcode Get Start...");
                    oSupportBarcodeList = oBarcodeRepo.GetBarcodes();
                    objInfoHandler.RaisInfo("Barcode Get End...");
                }
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }


        private void GetCGPMDetails()
        {
            try
            {
                //SQLiteCommand SqliteCmd = new SQLiteCommand("SELECT * FROM customer_group_product_map;", sqlite_con);
                //SqliteCmd.CommandTimeout = 100;

                //DataSet ds = new DataSet();
                //SQLiteDataAdapter da = new SQLiteDataAdapter(SqliteCmd);
                //da.Fill(ds);
                //da.Dispose();


                //if (ds != null && ds.Tables[0].Rows.Count > 0)
                //{
                //    var tempCgpmList = ds.Tables[0].AsEnumerable()
                //                 .Select(dataRow => new Customer_Group_Product_Map
                //                 {
                //                     customer_group_id = dataRow.Field<int>("customer_group_id"),
                //                     product_id = dataRow.Field<int>("product_id"),
                //                     margin_type = dataRow.Field<string>("margin_type"),
                //                     value_mode = dataRow.Field<string>("value_mode"),
                //                     product_or_margin_value = dataRow.Field<float>("product_or_margin_value"),
                //                     modified_on = dataRow.Field<DateTime>("modified_on")                                     
                //                 }).ToList();

                //    oSupportCGPMList = tempCgpmList;
                //}

                using (Customer_Group_Product_Map_Repo oCgpmRepo = new Customer_Group_Product_Map_Repo(GetSupportDbPath(), GetSqlitePlatform()))
                {
                    objInfoHandler.RaisInfo("CGPM Get Start...");
                    oSupportCGPMList = oCgpmRepo.GetCustomerGroupProductMap();
                    objInfoHandler.RaisInfo("CGPM Get End...");
                }
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }


        private void GetSKUDetails()
        {
            try
            {
                //SQLiteCommand SqliteCmd = new SQLiteCommand("SELECT * FROM stock_keeping_unit;", sqlite_con);
                //SqliteCmd.CommandTimeout = 100;

                //DataSet ds = new DataSet();
                //SQLiteDataAdapter da = new SQLiteDataAdapter(SqliteCmd);
                //da.Fill(ds);
                //da.Dispose();


                //if (ds != null && ds.Tables[0].Rows.Count > 0)
                //{
                //    var tempSkuList = ds.Tables[0].AsEnumerable()
                //                 .Select(dataRow => new Stock_Keeping_Unit
                //                 {
                //                     stock_keeping_unit_id = dataRow.Field<int>("stock_keeping_unit_id"),
                //                     modified_on = dataRow.Field<DateTime>("modified_on"),
                //                     stock_keeping_unit_no = dataRow.Field<string>("stock_keeping_unit_no"),
                //                     product_id = dataRow.Field<int>("product_id"),
                //                     variance_id = dataRow.Field<int>("variance_id"),
                //                     mrp_id = dataRow.Field<int>("mrp_id"),
                //                     landing_price = dataRow.Field<float>("landing_price"),
                //                     avg_landing_price = dataRow.Field<float>("avg_landing_price"),
                //                     is_server_synced = dataRow.Field<bool>("is_active")
                //                 }).ToList();

                //    oSupportSKUList = tempSkuList;
                //}

                using (SKURepo oSKURepo = new SKURepo(GetSupportDbPath(), GetSqlitePlatform()))
                {
                    objInfoHandler.RaisInfo("SKU Get Start...");
                    oSupportSKUList = oSKURepo.GetSKU();
                    objInfoHandler.RaisInfo("SKU Get End...");
                }
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }


        private void InsertProduct()
        {
            try
            {
                if(oSupportProductList != null && oSupportProductList.Count > 0)
                {
                    //foreach(Product oPro in oSupportProductList)
                    //{
                    //    string strProd = "INSERT INTO Product( product_id, product_name, product_desc, output_tax, product_img, modified_on, brand_id, pack_size_id, gst, c_gst, s_gst, i_gst, ut_gst, hsn_code, category_id, unit_id, standard_brand_discount, is_active, status, current_stock, brand_name, category_name, pack_size_name, unit_name, unit_short_code, location_product_status, reward_points_applicable, apply_credit_card_charges, meal_pass_allowed, product_print_label, margin_type, cess )";
                    //    strProd += "VALUES( " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", ";
                    //    strProd += " " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + ", " + oPro.product_id + " );";

                    //    SQLiteCommand SqliteCmd = new SQLiteCommand("SELECT * FROM stock_keeping_unit;", sqlite_con);
                    //    SqliteCmd.CommandTimeout = 100;
                    //    SqliteCmd.ExecuteNonQuery();                        

                    //}

                    using (ProductRepo oProdRepo = new ProductRepo(GetSourceDbPath(), GetSqlitePlatform()))
                    {
                        objInfoHandler.RaisInfo("Product Insert Start...");
                        oSourceProductList = oProdRepo.BulkAddOrUpdatePro(oSupportProductList);
                        objInfoHandler.RaisInfo("Product Insert End...");
                    }

                }
                
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }

        private void InsertMRP()
        {
            try
            {
                if (oSupportMRPList != null && oSupportMRPList.Count > 0)
                {
                    using (MRPRepo oMRPRepo = new MRPRepo(GetSourceDbPath(), GetSqlitePlatform()))
                    {
                        objInfoHandler.RaisInfo("MRP Insert Start...");
                        oSourceMRPList = oMRPRepo.BulkAddOrUpdateMRP(oSupportMRPList);
                        objInfoHandler.RaisInfo("MRP Insert End...");
                    }
                }

            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }

        private void InsertBarcode()
        {
            try
            {
                if (oSupportBarcodeList != null && oSupportBarcodeList.Count > 0)
                {
                    using (BarcodeRepo oBarRepo = new BarcodeRepo(GetSourceDbPath(), GetSqlitePlatform()))
                    {
                        objInfoHandler.RaisInfo("Barcode Insert Start...");
                        oSourceBarcodeList = oBarRepo.BulkAddOrUpdateBar(oSupportBarcodeList);
                        objInfoHandler.RaisInfo("Barcode Insert End...");
                    }
                }

            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }

        private void InsertCGPM()
        {
            try
            {
                if (oSupportCGPMList != null && oSupportCGPMList.Count > 0)
                {
                    using (Customer_Group_Product_Map_Repo oCgpmRepo = new Customer_Group_Product_Map_Repo(GetSourceDbPath(), GetSqlitePlatform()))
                    {
                        objInfoHandler.RaisInfo("CGPM Insert Start...");
                        int iSuccess = oCgpmRepo.BulkAddOrUpdateCustGrp(oSupportCGPMList);
                        objInfoHandler.RaisInfo("CGPM Insert End...");
                    }
                }

            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }

        private void InsertSku()
        {
            try
            {
                if (oSupportSKUList != null && oSupportSKUList.Count > 0)
                {
                    using (SKURepo oSKURepo = new SKURepo(GetSourceDbPath(), GetSqlitePlatform()))
                    {
                        objInfoHandler.RaisInfo("SKU Insert Start...");
                        int iSuccess = oSKURepo.BulkAddOrUpdateSKU(oSupportSKUList);
                        objInfoHandler.RaisInfo("SKU Insert End...");
                    }
                }
            }
            catch (Exception ex)
            {
                string strMsg = "Error message : " + Environment.NewLine + "Date : " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") + Environment.NewLine + "Error : " + ex.Message + Environment.NewLine;
                Logger objErrorHandler = new Logger();
                objErrorHandler.MsgType = MessageType.MailAndEventLog;
                objErrorHandler.RaiseError(strMsg, ex);
            }
        }

    }
}
